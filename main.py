__author__ = 'frm'

import socket
import sys
import SNMPMessage
#import pysnmp


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_address = ('0.0.0.0', 161)
print >>sys.stderr, "starting up on %s on port %s" % server_address
sock.bind(server_address)
while True:
    print >>sys.stderr, "\nwaiting to receive message"
    data, address = sock.recvfrom(4096)
    print >>sys.stderr, "received %s bytes from %s" % (len(data), address)
    if data:
        print data
        host_msg = SNMPMessage.SNMPMessage.fromCharString(data)
        print host_msg
        msg = SNMPMessage.SNMPMessage()
        msg.setRequestType("get-response")
        msg.setRequestID(host_msg["pdu"]["request-id"])
        if host_msg["pdu"]["request"] == 160 or host_msg["pdu"]["request"] == 161:
            for vbinding in host_msg["pdu"]["variable-bindings"]:
                #pwg (PRINTER-PORT-MONITOR-MIB)
                # ppmMib
                if vbinding["name"] == "1.3.6.1.4.1.2699.1.2":
                    value = "en-USerox;CMD:PCL, PJL, PostScript;MDL:WorkCentre 5955;CLS:Printer;DES:Xerox WorkCentre 5955;CID:XR_PS_Office_Mono"
                    msg.appendMIB("1.3.6.1.4.1.2699.1.2.1.1.1.0", value, "OCTET_STRING")
                    print SNMPMessage.SNMPMessage.toCharString(msg.message)
                    print len(SNMPMessage.SNMPMessage.toCharString(msg.message))
                # ppmGeneralNumberOfPrinters
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.1.2.0":
                    value = 1
                    msg.appendMIB(vbinding["name"], value, "GAUGE32")
                # ppmPrinterName
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.2.1.1.2.1":
                    value = "Xerox_5955"
                    msg.appendMIB(vbinding["name"], value, "OCTET_STRING")
                # ppmPrinterIEEE1284DeviceId
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.2.1.1.3.1":
                    value = "MFG:Xerox;CMD:PCL, PJL, PostScript;MDL:WorkCentre 5955;CLS:Printer;DES:Xerox WorkCentre 5955;CID:XR_PS_Office_Mono"
                    msg.appendMIB(vbinding["name"], value, "OCTET_STRING")
                # ppmPrinterNumberOfPorts
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.2.1.1.4.1":
                    value = 2
                    msg.appendMIB(vbinding["name"], value, "GAUGE32")
                # ppmPrinterPreferredPortIndex
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.2.1.1.5.1":
                    value = 2
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                # ppmPrinterHrDeviceIndex
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.2.1.1.6.1":
                    value = 1
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                # ppmPrinterSnmpCommunityName
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.2.1.1.7.1":
                    value = "public"
                    msg.appendMIB(vbinding["name"], value, "OCTET_STRING")
                # ppmPrinterSnmpQueryEnabled
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.2.1.1.8.1":
                    value = 1
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                # ppmPortEnabled
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.3.1.1.2.1.1":
                    value = 1
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                # ppmPortServiceNameOrURI
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.3.1.1.4.1.1":
                    value = "BSD version of LPD over port  515, default queue name =  lp"
                    msg.appendMIB(vbinding["name"], value, "OCTET_STRING")
                # ppmPortProtocolType
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.3.1.1.5.1.1":
                    value = 8
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                # ppmPortLprByteCountEnabled
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.3.1.1.9.1.1":
                    value = 2
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                #
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.3.1.1.2.1.2":
                    value = 1
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                #
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.3.1.1.5.1.2":
                    value = 38
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                # ppmPortProtocolTargetPort
                elif vbinding["name"] == "1.3.6.1.4.1.2699.1.2.1.3.1.1.6.1.2":
                    value = 9100
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                #host (HOST-RESOURCES-MIB)
                #hrDeviceStatus
                elif vbinding["name"] == "1.3.6.1.2.1.25.3.2.1.5.1":
                    value = 3
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                #hrPrinterStatus
                elif vbinding["name"] == "1.3.6.1.2.1.25.3.5.1.1.1":
                    value = 1
                    msg.appendMIB(vbinding["name"], value, "INTEGER")
                #hrPrinterDetectedErrorState
                elif vbinding["name"] == "1.3.6.1.2.1.25.3.5.1.2.1":
                    value = '\x81\x04'
                    msg.appendMIB(vbinding["name"], value, "OCTET_STRING")
            if len(msg.message["pdu"]["variable-bindings"]) > 0:
                sock.sendto(''.join(SNMPMessage.SNMPMessage.toCharString(msg.message)), address)
                print msg.message












