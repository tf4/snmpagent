__author__ = 'frm'

from math import log
from math import pow
import sys


class SNMPMessage:

    def __init__(self, version=0, community="public"):
        self.message = {"header": {"version": version, "community": community},
                        "pdu": {"request": 161, "request-id": 0, "error-status": 0, "error-index": 0,
                                "variable-bindings": []}
                        }

    def setCommunity(self, community):
        self.message["header"]["community"] = community

    def setRequestType(self, req_type):
        if req_type == "get-request":
            self.message["pdu"]["request"] = 160
        elif req_type == "get-next-request":
            self.message["pdu"]["request"] = 161
        elif req_type == "get-response":
            self.message["pdu"]["request"] = 162

    def setRequestID(self, req_id):
        self.message["pdu"]["request-id"] = req_id

    def appendMIB(self, mibname, mibvalue, value_type):
        self.message["pdu"]["variable-bindings"].append({"name": mibname, "valtype": value_type, "value": mibvalue})

    @staticmethod
    def fromCharString(msg):
        if msg[0] is not chr(48):
            return {"error": "invalid snmp packet"}
        total_len, chars_to_strip = ASN.decodeLength(msg[1:])
        if len(msg[1+chars_to_strip:]) != total_len:
            return {"error": "snmp packet is not complete"}
        chrstr = msg[1+chars_to_strip:]
        version, asn_type, chars_to_strip = ASN.decodeFirstOccurrence(chrstr)
        if version not in range(3):
            return {"error": "invalid version"}
        chrstr = chrstr[chars_to_strip:]
        community, asn_type, chars_to_strip = ASN.decodeFirstOccurrence(chrstr)
        if asn_type is not "OCTET_STRING":
            return {"error": "invalid community"}
        chrstr = chrstr[chars_to_strip:]
        if chrstr[0] not in [chr(160), chr(161), chr(162), chr(163), chr(164)]:
            return {"error": "invalid request type"}
        request = int(ord(chrstr[0]))
        request_len, chars_to_strip = ASN.decodeLength(chrstr[1:])
        chrstr = chrstr[1+chars_to_strip:]
        [request_id, asn_type, chars_to_strip] = ASN.decodeFirstOccurrence(chrstr)
        if asn_type is not "INTEGER":
            return {"invalid request id"}
        chrstr = chrstr[chars_to_strip:]
        [error_status, asn_type, chars_to_strip] = ASN.decodeFirstOccurrence(chrstr)
        if asn_type is not "INTEGER":
            return {"error": "invalid error status"}
        chrstr = chrstr[chars_to_strip:]
        [error_index, asn_type, chars_to_strip] = ASN.decodeFirstOccurrence(chrstr)
        if asn_type is not "INTEGER":
            return {"error": "invalid error index"}
        chrstr = chrstr[chars_to_strip:]
        if chrstr[0] is not chr(48):
            return {"error": "invalid bindings"}
        bindings_len = int(ord(chrstr[1]))
        if bindings_len != len(chrstr[2:]):
            return {"error": "invalid bindings length"}
        chrstr = chrstr[2:]
        if chrstr[0] is not chr(48):
            return {"error": "invalid mib object"}
        chrstr = chrstr[1:]
        mibs = ''.join(chrstr).split(chr(48))
        var_bindings = []
        for mib in mibs:
            length, chars_to_strip = ASN.decodeLength(mib)
            [mibvalue, mibtype, miblength] = ASN.decodeFirstOccurrence(mib[chars_to_strip:])
            if mibtype is "OBJECT_IDENTIFIER":
                obj_name = mibvalue
                [obj_value, obj_value_type, obj_value_len] = ASN.decodeFirstOccurrence(mib[chars_to_strip+miblength:])
                var_bindings.append({"name": obj_name, "value": obj_value, "valtype": obj_value_type})
        return {"header": {"version": version, "community": community},
                "pdu": {"request": request, "request-id": request_id,
                        "error-status": error_status, "error-index": error_index,
                "variable-bindings": var_bindings}}

    @staticmethod
    def toCharString(data):
        chrstr = [chr(48)]
        total_length_index = 1
        chrstr += ASN.encodeToASN(data["header"]["version"], "INTEGER")
        chrstr += ASN.encodeToASN(data["header"]["community"], "OCTET_STRING")
        chrstr.append(chr(data["pdu"]["request"]))
        #chrstr.append(chr(0))
        request_length_index = len(chrstr)
        chrstr += ASN.encodeToASN(data["pdu"]["request-id"], "INTEGER")
        chrstr += ASN.encodeToASN(data["pdu"]["error-status"], "INTEGER")
        chrstr += ASN.encodeToASN(data["pdu"]["error-index"], "INTEGER")
        chrstr.append(chr(48))
        #chrstr.append(chr(0))
        var_bindings_length_index = len(chrstr)
        for i in range(len(data["pdu"]["variable-bindings"])):
            chrstr.append(chr(48))
            mibchrstr = ASN.encodeToASN(data["pdu"]["variable-bindings"][i]["name"], "OBJECT_IDENTIFIER")
            mibvalue = ASN.encodeToASN(data["pdu"]["variable-bindings"][i]["value"],
                                       data["pdu"]["variable-bindings"][i]["valtype"])
            #chrstr.append(chr(len(mibchrstr) + len(mibvalue)))
            chrstr[len(chrstr):len(chrstr)] = ASN.encodeLength(len(mibchrstr) + len(mibvalue))
            chrstr += mibchrstr
            chrstr += mibvalue
            chrstr[var_bindings_length_index:var_bindings_length_index] = \
                ASN.encodeLength(len(chrstr[var_bindings_length_index:]))
            #chrstr[var_bindings_length_index] = chr(len(chrstr[var_bindings_length_index+1:]))
        chrstr[request_length_index:request_length_index] = ASN.encodeLength(len(chrstr[request_length_index:]))
        chrstr[total_length_index:total_length_index] = ASN.encodeLength(len(chrstr[total_length_index:]))
        return chrstr


class ASN:

    def __init__(self):
        pass

    @staticmethod
    def encodeLength(length):
        if length < 128:
            return [chr(length)]
        sizeinbytes = int(log(length, 256)) + 1
        chrstr = [chr(sizeinbytes | 128)]
        for i in range(sizeinbytes, 0, -1):
            chrstr.append(chr((length >> 8*(i-1)) & 255))
        return chrstr

    @staticmethod
    def decodeLength(lenchrstr):
        firstbyte = int(ord(lenchrstr[0]))
        if firstbyte & 128:
            length = firstbyte & 127
            decodedlen = 0
            for i in range(length):
                if i != 0:
                    decodedlen <<= 8
                decodedlen |= int(ord(lenchrstr[i+1]))
            return [decodedlen, length+1]
        else:
            return [firstbyte, 1]


    @staticmethod
    def decodeFirstOccurrence(data):
        if data[0] == chr(2):
            asn_type = "INTEGER"
            length = int(ord(data[1]))
            value = 0
            for i in range(2, 2+length):
                value |= ((int(ord(data[i])) * int(pow(256, (length+1-i)))) & (255*int(pow(256, (length+1-i)))))
            return [value, asn_type, length+2]
        if data[0] == chr(3):
            return ["", "BIT_STRING", int(ord(data[1]))+2]
        if data[0] == chr(4):
            asn_type = "OCTET_STRING"
            length = int(ord(data[1]))
            value = ""
            for i in range(2, 2+length):
                value = value + data[i]
            return [value, asn_type, length+2]
        if data[0] == chr(5):
            return [None, "NULL", 2]
        if data[0] == chr(6):
            asn_type = "OBJECT_IDENTIFIER"
            length = int(ord(data[1]))
            value = str(int(ord(data[2]))/40) + "." + str(int(ord(data[2])) - 40)
            digits = []
            shifter = 0
            for i in data[3:3+length-1]:
                if int(ord(i)) < 128 and shifter == 0:
                    digits.append(int(ord(i)))
                    #shifter = 0
                else:
                    # check here for explanation: https://msdn.microsoft.com/en-us/library/bb540809(v=vs.85).aspx
                    if shifter > 0:
                        digits[-1] = ((((digits[-1] >> 1) & 15) << 8) | (int(ord(i)) & 127)) | (digits[-1] & 128)
                    else:
                        digits.append(int(ord(i)))
                    if int(ord(i)) > 127:
                        shifter += 1
                    else:
                        shifter = 0
            for i in digits:
                value += '.' + str(i)
            return [value, asn_type, length+2]
        if data[0] == chr(66):
            asn_type = "GAUGE32"
            length = int(ord(data[1]))
            value = 0
            for i in range(2, 2+length):
                value |= ((int(ord(data[i])) * int(pow(256, (length+1-i)))) & (255*int(pow(256, (length+1-i)))))
            return [value, asn_type, length+2]
        else:
            return [None, None, None]

    @staticmethod
    def encodeToASN(value, valtype):
        if valtype is "INTEGER":
            chrstr = [chr(2)]
            length = 1
            if value != 0:
                length = int(log(value, 256)) + 1
            chrstr.append(chr(length))
            for i in range(length, 0, -1):
                chrstr.append(chr((value >> 8*(i-1)) & 255))
            return chrstr
        if valtype is "OCTET_STRING":
            chrstr = [chr(4), chr(len(value))]
            for c in value:
                chrstr.append(c)
            return chrstr
        if valtype is "NULL":
            return [chr(5), chr(0)]
        if valtype == "OBJECT_IDENTIFIER":
            chrstr = [chr(6), chr(0)]
            digits = value.split(".")
            chrstr.append(chr(40*int(digits[0]) + int(digits[1])))
            for digit in digits[2:]:
                val = int(digit)
                length = 1
                if val != 0:
                    length = int(log(val, 256)) + 1
                if val > 127:
                    right_nib = val << 1
                    right_nib &= 8064*(int(pow(256, length-2)))
                    mask = (int(pow(2, 8*length))-1) - (8064*(int(pow(256, length-2))))
                    val &= mask
                    val |= right_nib
                    val |= 128 * int(pow(2, 8*(length-1)))
                    val &= (int(pow(2, 8*length)) - 129)
                    for i in range(length, 0, -1):
                        chrstr.append(chr((val >> (8*(i-1))) & 255))
                    chrstr[-1] = chr(int(ord(chrstr[-1])) & 127)
                else:
                    chrstr.append(chr(val))
            chrstr[1] = chr(len(chrstr[2:]))
            return ''.join(chrstr)
        if valtype is "GAUGE32":
            chrstr = [chr(66)]
            length = 1
            if value != 0:
                length = int(log(value, 256)) + 1
            chrstr.append(chr(length))
            for i in range(length, 0, -1):
                chrstr.append(chr((value >> 8*(i-1)) & 255))
            return chrstr



